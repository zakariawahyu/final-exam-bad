/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package no1.inheritance;

/**
 *
 * @author zakariawahyu
 */
public class Computer {
    public int memory;
    public int hardisk;
    
    public Computer(int memory, int hardisk){
        this.memory = memory;
        this.hardisk = hardisk;
    }
    
    public void tambahMemory(int increment){
        memory += increment;
    }
    
    public String toString(){
    return ("Jumlah memory: " + memory + "\n"
    + "JUmlah hardisk: " + hardisk);
    }
}
