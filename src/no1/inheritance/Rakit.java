/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package no1.inheritance;

/**
 *
 * @author zakariawahyu
 */
public class Rakit extends Computer{
    
    public int seatHeight;
    
    public Rakit(int memory, int hardisk, int startHeight) {
        super(memory, hardisk);
        seatHeight = startHeight;
    }
   
    public void setHeight(int newValue) {
        seatHeight = newValue;
    }
 
    @Override public String toString(){
        return (super.toString() + "\nseat height is "
        + seatHeight);
    }
    
}
