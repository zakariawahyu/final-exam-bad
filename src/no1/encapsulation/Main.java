/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package no1.encapsulation;

/**
 *
 * @author zakariawahyu
 */
public class Main {
    public static void main(String[] args) {
        Computer c = new Computer();
        c.setCpu_Name("INTEL");
        c.setHardisk_Name("SEAGATE");
        c.setMemory_Name("KINGSTRON");
        c.setCasing_Name("BARACUDA");
        
        System.out.println("Computer Data :" + "\nCPU Name:" + c.getCpu_Name() + "\nHardisk Name:" + c.getHardisk_Name() + "\nMemory Name:" + c.getMemory_Name() + "\nCasing Name:" + c.getCasing_Name());
    }
}
