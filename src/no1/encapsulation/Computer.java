/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package no1.encapsulation;

/**
 *
 * @author zakariawahyu
 */
public class Computer {
    private String Cpu_Name;
    private String Memory_Name;
    private String Hardisk_Name;
    private String Casing_Name;

    public String getCpu_Name() {
        return Cpu_Name;
    }

    public void setCpu_Name(String Cpu_Name) {
        this.Cpu_Name = Cpu_Name;
    }

    public String getMemory_Name() {
        return Memory_Name;
    }

    public void setMemory_Name(String Memory_Name) {
        this.Memory_Name = Memory_Name;
    }

    public String getHardisk_Name() {
        return Hardisk_Name;
    }

    public void setHardisk_Name(String Hardisk_Name) {
        this.Hardisk_Name = Hardisk_Name;
    }

    public String getCasing_Name() {
        return Casing_Name;
    }

    public void setCasing_Name(String Casing_Name) {
        this.Casing_Name = Casing_Name;
    }
    
    
}
